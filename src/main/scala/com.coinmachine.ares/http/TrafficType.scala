package com.coinmachine.ares.http

/**
 * Created by bobo on 16/1/3.
 */
abstract class TrafficType(val uriSuffix: String, val id: Int)
object TrafficType {
  case object Unknown extends TrafficType("/", -1)
  case object StockTrade extends TrafficType("/trade/stock", 0)
  case object FutureTrade extends TrafficType("/trade/future", 1)


  def trafficType(uri: String): TrafficType = {
    uri match {
      case uri if uri.startsWith(StockTrade.uriSuffix) => TrafficType.StockTrade
      case uri if uri.startsWith(FutureTrade.uriSuffix) => TrafficType.FutureTrade
      case _ => TrafficType.Unknown
    }
  }
}
