package com.coinmachine.ares.util

import com.alibaba.fastjson.{JSON, JSONObject}
import com.coinmachine.ares.Logging
import com.coinmachine.ares.trade.UserInfo


/**
 * Created by bobo on 16/1/3.
 */
object LoopQuery extends Logging {

  def futurePosition(user: UserInfo, symbol: String, contractType: String, retryTimes: Int = 100000): JSONObject = {
    for (retry <- 0 to retryTimes) {
      try {
        val pos = JSON.parse(user.futurePostV1.future_position(symbol, contractType)).asInstanceOf[JSONObject]
        if (Option(pos).nonEmpty) {
          logDebug(s"Future position(symbol:$symbol, contractType:$contractType): $pos")
          return pos
        } else {
          logInfo(s"Illegal future position(symbol:$symbol, contractType:$contractType):$pos")
        }
      } catch {
        case e: Exception =>
          if (retry % 1000 == 1) logWarn(s"Can't get future position(symbol:$symbol, contractType:$contractType)", e)
          else logDebug(s"Can't get future position(symbol:$symbol, contractType:$contractType)")
      }
    }
    null
  }

  def futureUserInfo(user: UserInfo, retryTimes: Int = 100000): JSONObject = {
    for (retry <- 0 to retryTimes) {
      try {
        val userInfo = JSON.parse(user.futurePostV1.future_userinfo()).asInstanceOf[JSONObject]
        if (userInfo.containsKey("info")) {
          logDebug(s"userInfo:$userInfo")
          return userInfo.getJSONObject("info")
        } else {
          logInfo("Illegal user info:" + userInfo)
        }
        Thread.sleep(1000)
      } catch {
        case e: Exception =>
          if (retry % 1000 == 1) logWarn(s"Can't get User Info. Retry($retry)", e)
          else logDebug(s"Can't get User info. Retry($retry)")
      }
    }
    null
  }

  def stockOrderInfo(user: UserInfo, symbol: String, orderId: String, retryTimes: Int = 100000): JSONObject = {
    var retry = 0
    var order: JSONObject = null
    while (retry < retryTimes) {
      retry += 1
      try {
        val orders = JSON.parse(user.stockPostV1.order_info(symbol, orderId)).asInstanceOf[JSONObject].getJSONArray("orders")
        Option(orders) match {
          case Some(m) =>
            if (m.getJSONObject(0).getString("order_id") == orderId) {
              retry = retryTimes
              order = m.getJSONObject(0)
            } else {
              logInfo("Order id" + m.getJSONObject(0).getString("order_id") + " is not Expected:" + orderId)
            }
          case None =>
        }
        Thread.sleep(1000)
      } catch {
        case e: Exception =>
          if (retry % 1000 == 1) logWarn("Can't get Order Info for:" + orderId, e)
          else logInfo("Can't get Order info for:" + orderId)
      }
    }
    order
  }

  def futureOrderInfo(user: UserInfo, symbol: String, contractType: String, orderId: String, retryTimes: Int = 100000): JSONObject = {
    for (retry <- 0 to retryTimes) {
      try {
        val orders = JSON.parse(user.futurePostV1.future_orders_info(symbol, contractType, orderId)).asInstanceOf[JSONObject].getJSONArray("orders")
        Option(orders) match {
          case Some(m) =>
            if (m.getJSONObject(0).getString("order_id") == orderId) {
              return m.getJSONObject(0)
            } else {
              logInfo("Order id" + m.getJSONObject(0).getString("order_id") + " is not Expected:" + orderId)
            }
          case None =>
        }
        Thread.sleep(1000)
        logInfo(s"orders:$orders")
      } catch {
        case e: Exception =>
          if (retry % 1000 == 1) logWarn(s"Can't get Order info for: $orderId", e)
          else logInfo(s"Can't get Order info for: $orderId")
      }
    }
    return null
  }

  def placeStockOrder(user: UserInfo, symbol: String, placeType: String, price: Float, amount: Float, retryTimes: Int = 100000): String = {
    var retry = 0
    var orderId: String = null
    while (retry < retryTimes) {
      retry += 1
      try {
        val tmpOrderId = JSON.parse(user.stockPostV1.trade(symbol, placeType, price.toString, amount.toString))
          .asInstanceOf[JSONObject].getString("order_id")
        Option(tmpOrderId) match {
          case Some(m) =>
            retry = retryTimes
            orderId = tmpOrderId
          case None =>
            logInfo("Can't get orderId, re-place an order")
        }
        Thread.sleep(1000)
      } catch {
        case e: Exception =>
          if (retry % 1000 == 1) logWarn("Can't place an order for:" + orderId, e)
          else logInfo("Can't get Order info for:" + orderId)
      }
    }
    Option(orderId) match {
      case Some(id) =>
        id
      case _ =>
        throw new IllegalStateException("Can't place an order")
    }
  }

  def placeFutureOrder(user: UserInfo, symbol: String, contractType: String, price: Float, amount: Float, ot: String, retryTimes: Int = 100000): String = {
    for (retry <- 0 to retryTimes) {
      try {
        Thread.sleep(100)
        val tmpOrderId = JSON.parse(user.futurePostV1.future_trade(symbol, contractType, price.toString, amount.toString, ot, "0"))
              .asInstanceOf[JSONObject].getString("order_id")
        logInfo(s"place future orderId:$tmpOrderId")
        Option(tmpOrderId) match {
          case Some(m) =>
            return m
          case _ =>
                throw new IllegalStateException("Can't place an order")
          case None =>
            logInfo("Can't get orderId, re-place an order")
        }
      } catch {
        case e: Exception =>
          if (retry % 1000 == 1) logWarn("Can't place an order", e)
          else logInfo("Can't place an order")
      }
    }
    return null
  }

  def futureTicker(user: UserInfo, symbol: String, contractType: String, retryTimes: Int = 10000): JSONObject = {
    for (retry <- 0 to retryTimes) {
      try {
        val ticker = JSON.parse(user.futureGetV1.future_ticker(symbol, contractType)).asInstanceOf[JSONObject]
        if (ticker.containsKey("ticker")) {
          logDebug(s"Ticker: $ticker")
          return ticker.getJSONObject("ticker")
        }
        Thread.sleep(100)
      } catch {
        case e: Exception =>
          if (retry % 1000 == 1) logWarn("Can't get future ticker.", e)
          else logInfo("Can't get future ticker.")
      }
    }
    return null
  }

  def cancelStockOrder(user: UserInfo, symbol: String, order: String, retryTimes: Int = 10000): Unit = {
    var retry = 0
    while (retry < retryTimes) {
      retry += 1
      try {
        val tmpOrderId = JSON.parse(user.stockPostV1.cancel_order(symbol, order))
          .asInstanceOf[JSONObject].getString("order_id")
        Thread.sleep(1000)

        Option(tmpOrderId) match {
          case Some(oid) if order == oid =>
            retry = retryTimes
            logInfo(s"Cancel order: $order")
          case None =>
            logInfo(s"Can't get orderId, re-cancel order: $order")
        }
      } catch {
        case e: Exception =>
          if (retry % 1000 == 1) logWarn(s"Can't calcel order: $order", e)
          else logInfo(s"Can't calcel order: $order")
      }
    }
  }
}
