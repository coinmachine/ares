package com.coinmachine.ares.util

import com.alibaba.fastjson.{JSON, JSONObject}
import com.coinmachine.ares.Logging
import com.coinmachine.ares.trade.{Exchange, UserInfo}

/**
 * Created by bobo on 16/4/25.
 */
object StrategyUtil extends Logging {

  def config(): (UserInfo, String, Float, Float, Float) = {
    val conf = new JSONObject()

    val exchange: Exchange = {
      print(Console.CYAN_B + "Choose an exchange,ok for okcoin.cn; okf for okcoin.com future; hb for huobi(okf): ")
      val x = readLine()
      x match {
        case "ok" => Exchange.OKCOIN
        case "okf" => Exchange.OKCOINCOM_FUTURE
        case _ =>
          Exchange.OKCOINCOM_FUTURE
      }
    }
    println(Console.MAGENTA + "Exchange you choose:" + exchange)

    val userInfo = {
      print(Console.GREEN_B + Console.RED + "Input your API key:")
      val apiKey = readLine()
      print(Console.YELLOW_B + Console.RED + "Input your Secret key:")
      val secretKey = readLine()
      new UserInfo(apiKey, secretKey, exchange)
    }

    val userInfoData = JSON.parse(userInfo.futurePostV1.future_userinfo()).asInstanceOf[JSONObject]
    Option(userInfoData) match {
      case Some(info) if !info.containsKey("error_code") =>
        logInfo(s"user info:$info")
      case _ =>
        logError(s"Error: $userInfoData, check whether your keys is correct and the market is match.")
        System.exit(-1)
    }

    val coinType: String = {
      print(Console.BLUE_B + Console.WHITE + "btc or ltc?(btc)")
      if (readLine().trim == "ltc") "ltc" else "btc"
    }

    println(Console.MAGENTA + "Coin type you choose:" + coinType)
    val distance: Float = {
      print(Console.CYAN_B + "Trade Distance(1.0):")
      try {
        readLine().trim.toFloat
      } catch {
        case e: Exception =>
          logWarn("Invalid trade distance giving. Use 1.0 by default.")
          1.0f
      }
    }
    println(Console.MAGENTA + "Trade Distance you choose:" + distance)

    val unit: Float = {
      print(Console.GREEN_B + "Trade Unit(1.0):")
      try {
        readLine.trim.toFloat
      } catch {
        case e: Exception =>
          logWarn("Invalid trade unit giving. Use 1.0 by default.")
          1.0f
      }
    }
    println(Console.MAGENTA + "Trade Unit you choose:" + unit)

    val srr: Float = {
      println(Console.YELLOW_B + "Stop Risk Rate(20.0):")
      try {
        readLine.trim.toFloat
      } catch {
        case e: Exception =>
          logWarn("Invalid Stop Risk Rate giving. Use 20.0 by default.")
          20.0f
      }
    }
    println(Console.MAGENTA + "Stop Risk Rate you choose:" + srr)

    (userInfo, coinType, distance, unit, srr)
  }
}
