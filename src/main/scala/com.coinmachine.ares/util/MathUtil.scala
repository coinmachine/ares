package com.coinmachine.ares.util

/**
 * Math Utility
 */
object MathUtil {
  /**
   * 获取头顶价格,处理浮点数精度问题.例如:3.921 - 0.1 预期为3.821,然事实为3.8209999999999997
   * @param base 基准价格
   * @param distance 加减距离
   * @param precision 精度,1000代表保留3位小数,100保留2位,1保留一位
   * @return 头顶价格
   */
  def upPrice(base: Double, distance: Double, precision: Int = 1000): Double = ((math floor (base * precision + distance * precision)) / precision)

  /**
   * 获取脚底价格.
   * @param base 基准价格
   * @param distance 加减距离
   * @param precision 精度
   * @return 脚底价格
   */
  def downPrice(base: Double, distance: Double, precision: Int = 1000): Double = ((math floor (base * precision - distance * precision)) / precision)

}
