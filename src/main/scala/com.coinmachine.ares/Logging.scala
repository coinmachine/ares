package com.coinmachine.ares

import org.apache.commons.logging.{Log, LogFactory}

/**
 * 用于log的工具型trait
 * @author wuyongbo
 * @since 1.0
 */
trait Logging {
  private[this] var log_ : Log = null

  protected def logName = {
    // Ignore trailing $'s in the class names for Scala objects
    this.getClass.getName.stripSuffix("$")
  }

  protected def log: Log = {
    if (log_ == null) {
      log_ = LogFactory.getLog(logName)
    }
    log_
  }

  protected def logInfo(msg: String): Unit = {
    if (log.isInfoEnabled) log.info(msg)
  }

  protected def logDebug(msg: String): Unit = {
    if (log.isDebugEnabled) log.debug(msg)
  }

  protected def logWarn(msg: String): Unit = {
    if (log.isWarnEnabled) log.warn(msg)
  }

  protected def logWarn(msg: String, e: Throwable): Unit = {
    if (log.isWarnEnabled) log.warn(msg, e)
  }

  protected def logFatal(msg: String): Unit = {
    if (log.isFatalEnabled) log.fatal(msg)
  }

  protected def logError(msg: String): Unit = {
    if (log.isErrorEnabled) log.error(msg)
  }

  protected def logError(msg: String, e: Throwable): Unit = {
    if (log.isErrorEnabled) log.error(msg, e)
  }

  protected def logTrace(msg: String): Unit = {
    if (log.isTraceEnabled) log.trace(msg)
  }
}
