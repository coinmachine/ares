package com.coinmachine.ares.trade.stock

import com.alibaba.fastjson.{JSONObject, JSON}
import com.coinmachine.ares.Logging
import com.coinmachine.ares.trade.{Exchange, UserInfo, Order, Strategy}
import com.coinmachine.ares.util.{StrategyUtil, LoopQuery}

import scala.collection.mutable

/**
 *
 */
class WaveCutter(val user: UserInfo) extends Strategy with Logging{
  val upPlace = new mutable.Stack[Order]()
  val downPlace = new mutable.Stack[Order]()
  val coinType = "ltc"
  val currency = "cny"
  val orderDistance = 0.01f
  val tradeUnit = 1F

  override def init(): Unit = {
    val ticker = JSON.parse(user.stockGetV1.ticker(coinType + "_" + currency)).asInstanceOf[JSONObject].getJSONObject("ticker")
    logInfo(ticker.toJSONString)
    Option(ticker) match {
      case Some(t) =>
        val buyOne = t.getFloatValue("buy")
        placeUpDownOrders(upPlace, downPlace, buyOne)
      case None =>
        logError("Can't get ticker.")
    }
  }

  def placeUpDownOrders(upPlace: mutable.Stack[Order], downPlace: mutable.Stack[Order], price: Float): Unit = {
    //check whether balance is enough to place an order at best.
    val downOrderPrice = price - orderDistance
    if (!downPlace.exists(o => o.price == downOrderPrice)) {
      val orderId = LoopQuery.placeStockOrder(user, coinType + "_" + currency, "buy", downOrderPrice, tradeUnit)

      downPlace.push(new Order(orderId, downOrderPrice))
      Thread.sleep(20L)
      logInfo(s"Place an buy order:$orderId with price:$downOrderPrice and amount:$tradeUnit")
    } else {
      logInfo(s"Already placed an buy order with price:" + downOrderPrice)
    }

    val upOrderPrice = price + orderDistance
    if (!upPlace.exists(o => o.price == upOrderPrice)) {
      val orderId = LoopQuery.placeStockOrder(user, coinType + "_" + currency, "sell", upOrderPrice, tradeUnit)
      upPlace.push(new Order(orderId, upOrderPrice))
      Thread.sleep(20L)
      logInfo(s"Place an buy order:$orderId with price:$upOrderPrice and amount:$tradeUnit")
    } else {
      logInfo(s"Already placed an buy order with price:" + upOrderPrice)
    }
  }

  def processDeals(): Unit = {
    try {
      if (upPlace.nonEmpty) {
        val order = upPlace.top
        logDebug("Top Order of upPlace:" + order)
        val orderInfo = LoopQuery.stockOrderInfo(user, coinType + "_" + currency, order.id)
        Option(orderInfo.getIntValue("status")) match {
          case Some(oi) =>
            Option(orderInfo.getIntValue("status")) match {
              case Some(x) if x == -1 => //already canceled,被人工取消了理应再放一次
                upPlace.pop()
              case Some(x) if x == 2 => //totally concluded
                upPlace.pop()
                val price = orderInfo.getFloatValue("price")
                placeUpDownOrders(upPlace, downPlace, price)
              case _ =>
            }
          case None =>
            logError(s"Can't get order info for $order")
        }
      } else {
        logWarn(s"upPlace is empty.$upPlace")
      }

      if (downPlace.nonEmpty) {
        val order = downPlace.top
        val orderInfo = LoopQuery.stockOrderInfo(user, coinType + "_" + currency, order.id)
        Option(orderInfo) match {
          case Some(oi) =>
            Option(orderInfo.getIntValue("status")) match {
              case Some(x) if x == -1 =>
                downPlace.pop()
              case Some(x) if x == 2 =>
                downPlace.pop()
                val price = orderInfo.getFloatValue("price")
                placeUpDownOrders(upPlace, downPlace, price)
              case _ =>
            }
          case None =>
            logError(s"Can't get order info for $order")
        }
      } else {
        logWarn(s"downPlace is empty:$downPlace")
      }
    } catch {
      case e: Exception =>
        logError("Exception when placing order", e)
    }
  }

  override def run(): Unit = {
    init()
    while (!shutDownFlag) {
      processDeals()
    }
  }
}

object WaveCutter {
  def main(args: Array[String]) {
    val (userInfo, coinType, distance, unit, srr) = StrategyUtil.config()
    val waveCutter = new WaveCutter(userInfo)
    new Thread(waveCutter).run()

  }
}
