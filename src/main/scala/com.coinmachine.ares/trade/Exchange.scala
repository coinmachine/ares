package com.coinmachine.ares.trade

sealed abstract class Exchange(val marketType: MarketType, val urlPreffix: String, val id: Int)

object Exchange {
  case object OKCOIN extends Exchange(MarketType.Stock, "https://www.okcoin.cn", 0)
  case object OKCOINCOM_STOCK extends Exchange(MarketType.Stock, "https://www.okcoin.com", 1)
  case object OKCOINCOM_FUTURE extends Exchange(MarketType.Future, "https://www.okcoin.com", 2)
  case object UNKNOWN extends Exchange(MarketType.Stock, "", -1)
}