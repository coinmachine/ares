package com.coinmachine.ares.trade.server

import com.coinmachine.ares.Logging
import com.coinmachine.ares.trade.server.TradeServerHandler
import io.netty.bootstrap.ServerBootstrap
import io.netty.channel.nio.NioEventLoopGroup
import io.netty.channel.socket.SocketChannel
import io.netty.channel.socket.nio.NioServerSocketChannel
import io.netty.channel.{ChannelInitializer, ChannelOption}
import io.netty.handler.codec.http.{HttpObjectAggregator, HttpRequestDecoder, HttpResponseEncoder}
/**
 * Created by bobo on 16/1/3.
 */
object TradeServer extends Logging{
  def start(port: Int): Unit = {
    val bossGroup = new NioEventLoopGroup
    val workerGroup = new NioEventLoopGroup(12)
    val b = {
      val tmp = new ServerBootstrap
      tmp.group(bossGroup, workerGroup)
        .channel(classOf[NioServerSocketChannel])
        .childHandler(new ChannelInitializer[SocketChannel] {
          override def initChannel(ch: SocketChannel): Unit = {
            ch.pipeline.addLast(new HttpResponseEncoder)
              .addLast(new HttpRequestDecoder)
              .addLast(new HttpObjectAggregator(65535))
              .addLast(new TradeServerHandler())
          }
        }).option[Integer](ChannelOption.SO_BACKLOG, 128)
        .childOption[java.lang.Boolean](ChannelOption.SO_KEEPALIVE, true)
      tmp
    }
    b.bind(port).sync().channel().closeFuture().sync()
  }

  def main(args: Array[String]) {
    val bidServer = TradeServer
    bidServer.start(8842)
  }
}
