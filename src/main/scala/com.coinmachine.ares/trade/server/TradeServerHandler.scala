package com.coinmachine.ares.trade.server

import com.coinmachine.ares.Logging
import com.coinmachine.ares.http.TrafficType
import io.netty.channel.{ChannelHandlerContext, ChannelInboundHandlerAdapter}
import io.netty.handler.codec.http.FullHttpRequest

import scala.collection.JavaConversions._

/**
 * Created by bobo on 16/1/3.
 */
class TradeServerHandler extends ChannelInboundHandlerAdapter with Logging {
  var request: FullHttpRequest = null

  override def channelRead(ctx: ChannelHandlerContext, msg: Object): Unit = {
    val inTime = System.currentTimeMillis()
    request = msg.asInstanceOf[FullHttpRequest]
    request.headers().entries().foreach(entry => logDebug("Request header:" + entry))

    logDebug("Request Content:" + request.content())
    val trafficType = TrafficType.trafficType(request.getUri);

    trafficType match {
      //stock trade traffic
      case TrafficType.StockTrade =>

      //其余请求返回403 Forbidden,或忽略
      case _ => logDebug("Undefine traffic:" + request.getUri)
    }
    val processTime = System.currentTimeMillis() - inTime
    if (processTime > 100) logWarn("process time:" + processTime)
  }

  override def channelReadComplete(ctx: ChannelHandlerContext): Unit = {
    ctx.flush()
    ctx.close()
  }

  override def exceptionCaught(ctx: ChannelHandlerContext, cause: Throwable): Unit = {
    logError("Trade Server Handler Exception.", cause)
    ctx.close()
  }
}
