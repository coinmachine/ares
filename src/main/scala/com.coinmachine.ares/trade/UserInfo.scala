package com.coinmachine.ares.trade

import com.okcoin.rest.future.IFutureRestApi
import com.okcoin.rest.future.impl.FutureRestApiV1
import com.okcoin.rest.stock.IStockRestApi
import com.okcoin.rest.stock.impl.StockRestApi

/**
 * Created by bobo on 16/1/3.
 */
class UserInfo(private[this] val apiKey: String, private[this] val secretKey: String, val exchange: Exchange) {
  private[ares] val futureGetV1: IFutureRestApi = new FutureRestApiV1(exchange.urlPreffix)
  private[ares] val futurePostV1: IFutureRestApi = new FutureRestApiV1(exchange.urlPreffix, apiKey, secretKey)
  private[ares] val stockGetV1: IStockRestApi = new StockRestApi(exchange.urlPreffix)
  private[ares] val stockPostV1: IStockRestApi = new StockRestApi(exchange.urlPreffix, apiKey, secretKey)
}
