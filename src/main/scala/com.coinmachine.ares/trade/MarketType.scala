package com.coinmachine.ares.trade
/**
 * Created by bobo on 16/1/3.
 */
sealed abstract class MarketType(val id: Int)
object MarketType {
  case object Future extends MarketType(0)
  case object Stock extends MarketType(1)
}
