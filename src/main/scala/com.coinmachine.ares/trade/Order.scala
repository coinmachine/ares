package com.coinmachine.ares.trade

/**
 * Created by bobo on 16/1/3.
 */
class Order(val id: String, val price: Float) {

  override def toString = s"Order(id=$id, price=$price)"
}
