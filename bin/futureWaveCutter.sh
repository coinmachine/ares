#!/usr/bin/env bash

if [ -z "${ARES_HOME}" ]; then
  export ARES_HOME="$(cd "`dirname "$0"`"/..; pwd)"
fi

# Find the java binary
if [ -n "${JAVA_ARES_HOME}" ]; then
  RUNNER="${JAVA_ARES_HOME}/bin/java"
else
  if [ `command -v java` ]; then
    RUNNER="java"
  else
    echo "JAVA_ARES_HOME is not set" >&2
    exit 1
  fi
fi

# Find assembly jar
DSP_ASSEMBLY_JAR=
if [ -f "${ARES_HOME}/lib" ]; then
  ASSEMBLY_DIR="${ARES_HOME}/lib"
else
  ASSEMBLY_DIR="${ARES_HOME}/target"
fi

num_jars="$(ls -1 "$ASSEMBLY_DIR" | grep "^ares.*with-dependencies.*\.jar$" | wc -l)"
if [ "$num_jars" -eq "0" -a -z "$DSP_ASSEMBLY_JAR" ]; then
  echo "Failed to find ares assembly in $ASSEMBLY_DIR." 1>&2
  echo "You need to build ares before running this program." 1>&2
  exit 1
fi

if [ -d "$ASSEMBLY_DIR" ]; then
  ASSEMBLY_JARS="$(ls -1 "$ASSEMBLY_DIR" | grep "^ares.*with-dependencies.*\.jar$" || true)"
  if [ "$num_jars" -gt "1" ]; then
    echo "Found multiple ares assembly jars in $ASSEMBLY_DIR:" 1>&2
    echo "$ASSEMBLY_JARS" 1>&2
    echo "Please remove all but one jar." 1>&2
    exit 1
  fi
fi

pwd
exec "${RUNNER}"  -cp "${ASSEMBLY_DIR}"/"${ASSEMBLY_JARS}" com.coinmachine.ares.trade.future.FutureWaveCutter "$@" >> ${ARES_HOME}/log/ares.log
